import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';


@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
 
})
export class PostsComponent implements OnInit {
  posts;
  isLoading:boolean = true;
  constructor(private _postsService:PostsService) { }

  DeleteFormPosts(postToDelete)
  {
      //this.posts.splice(this.posts.indexOf(postToDelete),1) // canceled after the class about firebase
      this._postsService.deletePost(postToDelete);
  }
  updatePosts(postToUpdate)
  {
    this._postsService.updatePoset(postToUpdate);
  }
  addpost(post)
  {
    this._postsService.addPost(post);
  }
  ngOnInit() 
  {
      this._postsService.getPosts().subscribe(postdata => {this.posts = postdata; this.isLoading = false;});
  }


}
